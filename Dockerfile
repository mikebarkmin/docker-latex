FROM debian:sid

RUN apt-get update && \
    apt-get install -y texlive-full && \
    apt-get install -y biber && \
    apt-get purge -y *-doc && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/*
    
CMD ["latexmk", "-pdf", "main.tex"]